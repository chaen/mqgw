import cyclone.web
import sys
from twisted.internet import reactor
from twisted.internet import ssl
from twisted.python import log
from twisted.internet.ssl import ContextFactory
from OpenSSL import SSL, crypto
import os

HOSTCERT='/etc/grid-security/hostcert.pem'
HOSTKEY='/etc/grid-security/hostkey.pem'
CAPATH='/etc/grid-security/certificates/'

def verifyCallback(connection, x509, errnum, errdepth, ok):
    if not ok:
        print 'invalid cert from subject:', x509.get_subject()
        return False
    else:
        print "Certs are fine:", x509.get_subject()
    return True


class MainHandler(cyclone.web.RequestHandler):
    def get(self):
        self.write("Hello, %s" % self.request.protocol)

class GridSSLContextFactory(ContextFactory):
    """ Here is the magic to allow proxy """
    def __init__(self, capath, cert, key):
        self.capath = capath
        self.cert = cert
        self.key = key

    def getContext(self):
        ctx = SSL.Context(SSL.TLSv1_METHOD)
        ctx.use_certificate_file(self.cert)
        ctx.use_privatekey_file(self.key)
        ctx.load_verify_locations(None, self.capath)
        # Here we go for the magic
        ctx.get_cert_store().set_flags(
            crypto.X509StoreFlags.ALLOW_PROXY_CERTS
        )
        ctx.set_verify(
                SSL.VERIFY_PEER | SSL.VERIFY_FAIL_IF_NO_PEER_CERT,
                verifyCallback
        )
        return ctx



def main():
    log.startLogging(sys.stdout)
    application = cyclone.web.Application([
        (r"/", MainHandler)
    ])

    capath = os.environ.get('CAPATH',CAPATH)
    hostcert = os.environ.get('HOSTCERT',HOSTCERT)
    hostkey = os.environ.get('HOSTKEY', HOSTKEY)
    print "Using %s %s %s"%(capath, hostcert,hostkey)

    interface = "0.0.0.0"
    reactor.listenSSL(8000, application,GridSSLContextFactory(capath, hostcert,hostkey),
                      interface=interface)
    reactor.run()


if __name__ == "__main__":
    main()
