var HOSTCERT='/etc/grid-security/hostcert.pem'
var HOSTKEY='/etc/grid-security/hostkey.pem'
var CAPATH='/etc/grid-security/allCAs.pem'

if (process.env.HOSTCERT){
  HOSTCERT=process.env.HOSTCERT;
} 

if (process.env.HOSTKEY){
  HOSTKEY=process.env.HOSTKEY;
} 

if (process.env.CAPATH){
  CAPATH=process.env.CAPATH;
} 

console.log('Using:');
console.log('HOSTCERT ' + HOSTCERT);
console.log('HOSTKEY ' + HOSTKEY);
console.log('CAPATH ' + CAPATH);





var fs = require('fs'); 
var https = require('https'); 
var options = { 
    key: fs.readFileSync(HOSTKEY), 
    cert: fs.readFileSync(HOSTCERT), 
    ca: fs.readFileSync(CAPATH), 
    requestCert: true, 
    rejectUnauthorized: true
}; 
https.createServer(options, function (req, res) { 
    console.log(new Date()+' '+ 
        req.connection.remoteAddress+' '+ 
        req.socket.getPeerCertificate().subject.CN+' '+ 
        req.method+' '+req.url); 
    res.writeHead(200); 
    res.end("hello world\n"); 
}).listen(8000);


