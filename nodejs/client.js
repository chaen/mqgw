var fs = require('fs'); 
var https = require('https'); 
var options = { 
    hostname: 'localhost', 
    port: 8000, 
    path: '/', 
    method: 'GET', 
    key: fs.readFileSync('/tmp/vmcert/userkeyNP.pem'), 
    cert: fs.readFileSync('/tmp/vmcert/usercert.pem'), 
    ca: fs.readFileSync('/tmp/vmcert/allCAs.pem') }; 
var req = https.request(options, function(res) { 
    res.on('data', function(data) { 
        process.stdout.write(data); 
    }); 
}); 
req.end(); 
req.on('error', function(e) { 
    console.error(e); 
});
