import unittest
from random import random
from funkload.FunkLoadTestCase import FunkLoadTestCase

class Simple(FunkLoadTestCase):
    """This test use a configuration file Simple.conf."""
    def setUp(self):
        """Setting up test."""
        self.server_url = self.conf_get('main', 'url')
        self.hostcert = self.conf_get('main', 'cert')
        self.hostkey = self.conf_get('main', 'key')
        self.setKeyAndCertificateFile(self.hostkey, self.hostcert)

    def test_simple(self):
        # The description should be set in the configuration file
        server_url = self.server_url
        # begin test ---------------------------------------------
        nb_time = self.conf_getInt('test_simple', 'nb_time')
        for i in range(nb_time):
            ret = self.get(server_url, description='Get URL')
            self.assert_(ret.code == 200, "Wrong return code %s"%ret.code)
        # end test -----------------------------------------------

if __name__ in ('main', '__main__'):
    unittest.main()
