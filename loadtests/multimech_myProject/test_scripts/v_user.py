
import random
import time
import requests

proxy='/tmp/proxy.pem'

class Transaction(object):
    def __init__(self):
        pass

    def run(self):
        st = time.time()
        res = requests.get('https://lbmesossl05.cern.ch:31073', cert = (proxy,proxy))
        latency = time.time() - st
        self.custom_timers['Example_Homepage'] = latency
        assert(res.status_code == 200), 'Bad Response: HTTP %s' % res.status_code


if __name__ == '__main__':
    trans = Transaction()
    trans.run()
    print trans.custom_timers
