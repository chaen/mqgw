import tornado.httpserver
import tornado.ioloop
import ssl
#from OpenSSL import SSL, crypto

import os

HOSTCERT='/etc/grid-security/hostcert.pem'
HOSTKEY='/etc/grid-security/hostkey.pem'
CAPATH='/etc/grid-security/certificates/'

capath = os.environ.get('CAPATH',CAPATH)
hostcert = os.environ.get('HOSTCERT',HOSTCERT)
hostkey = os.environ.get('HOSTKEY', HOSTKEY)

context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH, capath = capath)
#context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
context.verify_mode = ssl.CERT_REQUIRED
#context.verify_flags = ssl.VERIFY_CRL_CHECK_CHAIN
#context.load_cert_chain("/etc/grid-security/hostcert.pem",
#        "/etc/grid-security/hostkey.pem")
context.load_cert_chain(hostcert,
        hostkey)
#context.load_cert_chain("/home/chaen/dirac/etc/grid-security/hostcert.pem",
#        "/home/chaen/dirac/etc/grid-security/hostkey.pem")
#context.load_verify_locations(None,"/etc/grid-security/certificates")
#context.verify_flags =ssl.VERIFY_CRL_CHECK_CHAIN

#context = getContext("/home/chaen/dirac/etc/grid-security/hostcert.pem", "/home/chaen/dirac/etc/grid-security/hostkey.pem", "/etc/grid-security/certificates")
def handle_request(request):
  print("CHRIS %s"%type(request))
  print("CHRIS cert %s"%request.get_ssl_certificate())
  message = "You requested %s\n" % request.uri
  request.write("HTTP/1.1 200 OK\r\nContent-Length: %d\r\n\r\n%s" % (len(message), message))
  request.finish()


server = tornado.httpserver.HTTPServer(handle_request, ssl_options=context)
server.listen(8000)
tornado.ioloop.IOLoop.instance().start()
