import tornado

url = 'https://localhost:8000'

request = tornado.httpclient.HTTPRequest(url = url, method = "GET", 
        client_key="/home/chaen/.globus/userkey.pem",
        client_cert="/home/chaen/.globus/usercert.pem",
        ca_certs="/tmp/allCA.pem")
client = tornado.httpclient.AsyncHTTPClient()
param = client.fetch(request, self.handle_request)
