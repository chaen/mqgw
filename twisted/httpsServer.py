from OpenSSL import SSL, crypto
import os
import sys
from twisted.internet import ssl, reactor, endpoints
from twisted.internet.protocol import Factory, Protocol
from twisted.internet.ssl import ContextFactory
from twisted.web import server, resource



class Counter(resource.Resource):
    isLeaf = True
    numberRequests = 0

    def render_GET(self, request):
        self.numberRequests += 1
        request.setHeader(b"content-type", b"text/plain")
        content = u"I am request #{}\n".format(self.numberRequests)
        return content.encode("ascii")

class Echo(Protocol):
    def dataReceived(self, data):
        """As soon as any data is received, write it back."""
        self.transport.write(data)


def verifyCallback(connection, x509, errnum, errdepth, ok):
    if not ok:
        print 'invalid cert from subject:', x509.get_subject()
        return False
    else:
        print "Certs are fine:", x509.get_subject()
    return True


class GridSSLContextFactory(ContextFactory):
    def __init__(self, capath, cert, key):
        self.capath = capath
        self.cert = cert
        self.key = key

    def getContext(self):
        ctx = SSL.Context(SSL.TLSv1_METHOD)
        ctx.use_certificate_file(self.cert)
        ctx.use_privatekey_file(self.key)
        ctx.load_verify_locations(None, self.capath)
        ctx.get_cert_store().set_flags(
            crypto.X509StoreFlags.ALLOW_PROXY_CERTS
        )
        ctx.set_verify(
                SSL.VERIFY_PEER | SSL.VERIFY_FAIL_IF_NO_PEER_CERT,
                verifyCallback
        )
        return ctx



if __name__ == '__main__':
    #factory = Factory()
    #factory.protocol = Echo
    factory = server.Site(Counter())
    #factory.protocol = Echo
    reactor.listenSSL(8000, factory,
        GridSSLContextFactory(
            "/etc/grid-security/certificates",
            "/etc/grid-security/hostcert.pem",
            "/etc/grid-security/hostkey.pem"
        )
    )
    reactor.run()
                    
