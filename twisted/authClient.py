from OpenSSL import SSL, crypto
from twisted.internet import ssl, reactor
from twisted.internet.protocol import ClientFactory, Protocol

class EchoClient(Protocol):
    def connectionMade(self):
        print "hello, world"
        self.transport.write("hello, world!")

    def dataReceived(self, data):
        print "Server said:", data
        self.transport.loseConnection()

class EchoClientFactory(ClientFactory):
    protocol = EchoClient

    def clientConnectionFailed(self, connector, reason):
        print "Connection failed - goodbye!"
        reactor.stop()

    def clientConnectionLost(self, connector, reason):
        print "Connection lost - goodbye!"
        reactor.stop()

class CtxFactory(ssl.ClientContextFactory):
    def getContext(self):
        self.method = SSL.TLSv1_METHOD
        ctx = ssl.ClientContextFactory.getContext(self)
        #ctx.use_certificate_file('/home/chaen/.globus/usercert.pem')
        #ctx.use_privatekey_file('/home/chaen/.globus/userkeyNP.pem')
        #ctx.get_cert_store().set_flags(
        #    crypto.X509StoreFlags.ALLOW_PROXY_CERTS
        #)
        # The proxies are chained, so we cant use use_certificate_file
        ctx.use_certificate_chain_file('/tmp/proxy.pem')
        ctx.use_privatekey_file('/tmp/proxy.pem')

        return ctx

if __name__ == '__main__':
    factory = EchoClientFactory()
    reactor.connectSSL('localhost', 8000, factory, CtxFactory())
    reactor.run()

