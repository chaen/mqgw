The idea is to compare a few technologies as a gateway to ActiveMQ.

Twisted (python): quite low level.
Tornado (python): high level web Framework
Cyclone (python): intermediate level. Implement Tornado API as Twisted protocol
nodejs (javascript): well, nodeJS...


The "tricky" part is to make them work with proxy. I went with the assumption that
we will support RFC proxy only !
OpenSSL can be configured to accept RFC proxy, but it can be quite tricky (https://github.com/nodejs/node/blob/master/deps/openssl/openssl/crypto/x509/x509_vfy.c#L620 and https://github.com/nodejs/help/issues/788).

For twisted and cyclone, we can enable the proxy in the code. For the others, we need to run them with an environment variable.

In order to test them, I package each solution in a docker container, that I run on a dedicated machine (lbmesossl05) with mesos.
I give each of them the exact same resources, thus allowing for a fair comparison.
Each folder contains a Dockerfile on how to construct the container, and a json file on how it is run with marathon.

The test is then performed with funkload (`pip install funkload` is your friend)
It basically consists in hammering the service with different numbers of clients and so on.
In order to run it, you need a proxy. Generate it, and store it in /tmp/proxy.pem
For conveniency, I wrote a Makefile in loadtests/funkload. All the parameters you can change in the way funkload runs are at the beginning. It starts from Simple.tmpl and generate Simple.conf, which is needed to run the test. You can:
* specify one of the target (twisted cyclone nodejs tornado restdirac). This will run the test, and generate an html report in loadtests/funkload/reports
* generate differencial report (after running the test of course): `make diff twisted cyclone` for example. it generates a report in reports/A-VS-B
* generate ALL differencial reports (it expects ALL the tests have run): `make alldiff`

At the moment, all the server are quite dummy, they just answer stupid http request, and in different ways (some give back a number, some say Hello, etc).
One needs to:
* make them rest compatible, with the same behavior
* make them interact with ActiveMQ
